# TheGreatMcPain Wine Overlay

Lutris/Proton Wine Staging ebuilds based on [bobwya's `app-emulation/wine-staging`](https://github.com/bobwya/bobwya/tree/master/app-emulation/wine-staging) ebuild.

Ebuilds were originally from my Gentoo overlay `thegreatmcpain-overlay`.

This overlay does require the `bobwya` overlay which is available through layman, and eselect-repository.

## Contents of app-emulation/README.md from my original overlay

The `wine-staging` ebuilds are based on the wine-vanilla ebuild from [bobwya's overlay](https://github.com/bobwya/bobwya), but have the dependences from wine-staging added.

They've also been modified to pull from [lutris's wine repository,](https://github.com/lutris/wine)\
and each ebuild points to a different branch from that repository.

Here's the current ebuild versions, and there respective lutris/proton wine branch.

| wine-staging version | lutris wine branch                                                                                                                                          |
|----------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 6.14                 | [lutris-fshack-6.14-3 branch of https://github.com/lutris/wine](https://github.com/lutris/wine/tree/lutris-fshack-5.7) (includes Proton's fullscreen hacks) |
| 6.3                  | [proton_6.3 branch of https://github.com/ValveSoftware/Proton](https://github.com/ValveSoftware/Proton/tree/proton_6.3)                                     |

## Installation

### layman

```
layman -o https://gitlab.com/TheGreatMcPain/thegreatmcpain-wine-overlay/-/raw/master/repository.xml -f -a thegreatmcpain-wine
```

### Manual

```
[thegreatmcpain-wine]
sync-uri = https://gitlab.com/TheGreatMcPain/thegreatmcpain-wine-overlay.git
sync-type = git
location = /var/db/repos/thegreatmcpain-wine
```
